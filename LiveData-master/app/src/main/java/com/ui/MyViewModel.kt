package com.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.repos.RemoteRepository
import com.network.data.Country

class MyViewModel : ViewModel() {

    fun callAPI() : MutableLiveData<Country>
    {
        return RemoteRepository().callAPI()
    }
}